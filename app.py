from flask import Flask, jsonify,json
from boto.s3.connection import S3Connection
from flask.ext.cors import CORS
import pytz, time
from time import mktime
from datetime import datetime

local = pytz.timezone ("America/Sao_Paulo")
conn = S3Connection('AKIAIUBZI6ALTEM2HYEQ', '8tAYcyDAuOrGv64amwI+RGjZ6hgQGrM3zliaPrOi')
bucket = conn.get_bucket('xml-parceiros-hu')



app = Flask(__name__)
cors = CORS(app)

@app.route('/')
def index():
	rs = bucket.list()
	xmls = {"prod/allin/allin.xml":7200, "prod/criteo/ofertas.xml":7200, "prod/default/default.xml":7200, 
	"prod/globo/default.xml":7200,"prod/globo/globo-vitrine.xml":7200,"prod/google-criativo/products.xml":7200}

	errList = []
	sussList = []
	
	for key in rs:
   		name = key.name
   		
		if name in xmls:
			modified = time.strptime(key.last_modified[:19], "%Y-%m-%dT%H:%M:%S")
			dt = datetime.fromtimestamp(mktime(modified))
      
			dtdiff = (datetime.utcnow() - dt).total_seconds()
			if dtdiff > xmls[name]:
				errList.append({"parceiro":name,"data_modificacao":key.last_modified[:19]})
			else:
				sussList.append({"parceiro":name,"data_modificacao":key.last_modified[:19]})
	exitMap = {"success":sussList, "error":errList}
	return json.dumps(exitMap)


app.run(debug=True)
