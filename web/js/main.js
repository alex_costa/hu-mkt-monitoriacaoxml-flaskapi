var monitoriaApp = angular.module("monitoria", []);

monitoriaApp.controller("FlaskXMLCtrl", ['$scope','XMLStatusService', function($scope, XMLStatusService) {
        $scope.nome = "alex costa";
        XMLStatusService.getXMLStatus().success(function(data, status, headers, config) {
                $scope.xmlstatus = trataData(data);
                
            })
            .error(function (error) {
                $scope.status = 'Unable to load countries data: ' + error.message;
            });
        trataData = function(dados) {
            for(elm in dados) {
                console.log(dados[elm]);
                for(var i = 0; i < dados[elm].length; i++ ) {
                    dados[elm][i].parceiro = pegaNomeParceiro(dados[elm][i].parceiro);
                    dados[elm][i].data_modificacao = formataDataModificacao(dados[elm][i].data_modificacao);
                }
            }
            return dados;
        }
        
        pegaNomeParceiro = function(nome) {
            return nome.split("/")[1].substring(0,1).toUpperCase() + nome.split("/")[1].substring(1);   
        }
        
        formataDataModificacao = function(data_modificacao) {
            var d = new Date(data_modificacao);
            return (d.getDate() < 10 ? "0" : "") + d.getDate() + "/" + (d.getMonth() < 10 ? "0" : "") + d.getMonth() + "/" + d.getFullYear() + " " + d.getHours() + ":" + (d.getMinutes() < 10 ? "0" : "") +  d.getMinutes();
        }
}]);

monitoriaApp.service("XMLStatusService", ['$http', function($http) {
    this.getXMLStatus = function(data, status, headers, config) {
        return $http.get("http://localhost:5000");
    };
}]);